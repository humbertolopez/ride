<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php if(is_home()){ ?>
				<?php bloginfo('name'); ?> — <?php bloginfo('description'); ?>
			<?php } elseif (is_page()) { ?>
				<?php the_title(); ?> — <?php bloginfo('name'); ?> — <?php bloginfo('description'); ?>
			<?php } else if (is_category()) { ?>
				<?php $cat = get_category(get_query_var('cat')); echo $cat->name; ?> — <?php bloginfo('name'); ?> — <?php bloginfo('description'); ?>
			<?php } else if (is_single()) { ?>
				<?php the_title(); ?> — <?php bloginfo('name'); ?> — <?php bloginfo('description'); ?>
			<?php } 
		?>
	</title>
	<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
	<link href="https://fonts.googleapis.com/css?family=Dosis:600,400,300|Nunito:400,700" rel="stylesheet">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<?php wp_head(); ?>
	<?php
		if(is_singular()) {
			get_template_part('parts/fb-og');
			get_template_part('parts/json-ld');
		} else {
			get_template_part('parts/json-ld-home');
		}
	?>
	<?php 
		if(!is_user_logged_in()) {
			get_template_part('parts/tag-manager-header');
		}
	?>
</head>
<body>
	<?php 
		if(!is_user_logged_in()){
			get_template_part('parts/tag-manager-body');
			get_template_part('parts/facebook-sdk');
		}
	?>
	<?php 
		if(is_front_page() && !is_paged()){
			?>
				<header class="text-center">
					<a class="home-logo" href="<?php echo bloginfo('url'); ?>"><img class="ride-logo-big rounded-circle" src="<?php echo get_template_directory_uri(); ?>/img/ride_logo.svg"></a>
					<nav class="container">
						<?php wp_nav_menu(array('theme_location'=>'header-menu','menu_class'=>'navigation list-inline','container'=>'ul')); ?>
					</nav>
				</header>
				<?php get_template_part('parts/recent-posts'); ?>
			<?php
		} else {
			?>
				<header class="container">
					<div class="row small-menu-buffer align-items-center">
						<div class="col-lg-1 col-sm-12 col-xs-12">
							<a class="home-logo-small" href="<?php echo bloginfo('url'); ?>"><img class="ride-logo-small rounded-circle" src="<?php echo get_template_directory_uri(); ?>/img/ride_logo.svg"></a>
						</div>
						<nav id="no-home-nav" class="col-lg-11 col-sm-12 col-xs-12">
							<?php wp_nav_menu(array('theme_location'=>'header-menu','menu_class'=>'navigation list-inline navigation-buffer-cancel','container'=>'ul')); ?>
						</nav>
					</div>
				</header>
			<?php
		}
	?>