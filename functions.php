<?php 
	//menus principal y footer
	function register_menus() {
		register_nav_menus(
			array(
				'header-menu'=>__('Menú Principal'),
				'footer-first'=>__('Footer No. 1'),
				'footer-second'=>__('Footer No. 2'),
				'footer-third'=>__('Footer No. 3'),
				'subscribe-podcast'=>__('Suscripciones al Podcast')
			)
		);
	}
	add_action('init','register_menus');
	//activate thumbnails
	add_theme_support('post-thumbnails');
	//thumb para destacados 
	add_image_size('destacado',496,279,true);
	//thumb para general
	add_image_size('index-post',760,427,true);
	//responsive images
	function ride_content_image_sizes_attr($sizes, $size) {
	    $width = $size[0];
	    
	    if($width >= 1200){
	    	$sizes = '(max-width: 768px) 66vw, (max-width: 992px) 47vw, (max-width:1200px) 57vw, 695px';
	    } else if ($width >= 992) {
	    	$sizes = '(max-width: 768px) 66vw, (max-width: 992px) 47vw, (max-width:1200px) 57vw, 695px';
	    } else if ($width >= 768) {
	    	$sizes = '(max-width: 576px) 61w, (max-width: 768px) 66vw, 475px';
	    } else if ($width < 576) {
	    	$sizes = '(min-width: 576px) 86w, 495px';
	    }
	    return $sizes;
	}
	add_filter('wp_calculate_image_sizes', 'ride_content_image_sizes_attr', 10 , 2);
	//responsive thumbs
	function ride_post_thumbnail_sizes_attr($attr,$attachment,$size) {
		if($size === 'destacado'){
			$attr['sizes'] = '(max-width:576px) 30vw, (max-width:768) 33vw, (max-width:992px) 18vw, (max-width:1200px) 22vw, 496px';
		}
		return $attr;
	}
	add_filter('wp_get_attachment_image_attributes','ride_post_thumbnail_sizes_attr',10,3);
	//remove p tag to some elements in the_content
	function filter_ptags_on_images($content){
	    $content = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '\1', $content);
	    $content = preg_replace('/<p>\s*(<script.*>*.<\/script>)\s*<\/p>/iU', '\1', $content);
	    //$content = preg_replace('/<p>\s*(<iframe.*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
	    return $content;
	}
	add_filter('the_content', 'filter_ptags_on_images');
	//add outline class to pagination buttons
	function post_links_attrs(){
		return 'class="btn btn-outline-primary"';
	}
	add_filter('next_posts_link_attributes','post_links_attrs');
	add_filter('previous_posts_link_attributes','post_links_attrs');
?>