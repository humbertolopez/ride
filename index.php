<?php get_header(); ?>
	<!-- content -->
	<div id="ride-content" class="container border-top">
		<div class="row">
			<!-- main block -->
			<div class="col-lg-8 col-sm-12">
				<div class="row single-article-row">
					<?php get_template_part('parts/index-post'); ?>
				</div>
				<!-- pagination -->
				<?php if(have_posts()) : the_post(); ?>
					<div class="row single-article-row">
						<div class="col-6 single-buffer">
							<?php previous_posts_link('← Regresar'); ?>
						</div>
						<div class="col-6 single-buffer text-right">
							<?php next_posts_link( 'Más historias →' ); ?>
						</div>
					</div>
				<?php endif; ?>
				<!-- /pagination -->
			</div>
			<!-- /main block -->
			<!-- sidebar -->
			<div class="col-lg-4 col-sm-12">
				<div id="sidebar">
					<h5>¡Suscríbete a RIDE, el podcast!</h5>
					<?php wp_nav_menu(array('theme_location'=>'subscribe-podcast','menu_class'=>'list-unstyled','container'=>'ul')); ?>
				</div>
			</div>
			<!-- /sidebar -->
		</div>
	</div>
	<!-- /content -->
	<div class="container-fluid">
		<div class="row">
			<div class="col text-center ride-desc">
				<p>El blog y podcast sobre la cultura automotriz, desde México.</p>	
			</div>
		</div>
	</div>
<?php get_footer(); ?>