<!-- footer -->
	<footer class="container">
		<div class="row">
			<div class="col-lg-3 col-sm-6 col-xs-6">
				<h6 class="text-uppercase">Categorías principales</h6>
				<?php wp_nav_menu(array('theme_location'=>'footer-second','menu_class'=>'list-unstyled','container'=>'ul')); ?>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-6">
				<h6 class="text-uppercase">Suscríbete a RIDE</h6>
				<?php wp_nav_menu(array('theme_location'=>'footer-third','menu_class'=>'list-unstyled','container'=>'ul')); ?>
			</div>
		</div>
	</footer>
	<!-- footer -->
	<!-- scripts -->
	<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script type="text/javascript"src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<!-- scripts -->
	<?php wp_footer(); ?>
</body>
</html>