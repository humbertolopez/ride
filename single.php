<?php get_header(); ?>
	<!-- content -->
	<div id="ride-content-single-page" class="container border-top">
		<div class="row">
			<!-- main block -->
			<div class="col-lg-10 offset-lg-1 col-sm-12">
				<div class="row single-post-row">
					<?php get_template_part('parts/single-general'); ?>
				</div>
			</div>
			<!-- /main block -->
		</div>
	</div>
	<!-- /content -->
	<div class="container-fluid">
		<div class="row">
			<div class="col text-center ride-desc">
				<p>El blog y podcast sobre la cultura automotriz, desde México.</p>	
			</div>
		</div>
	</div>
<?php get_footer(); ?>