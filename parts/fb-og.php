<meta property="fb:app_id" content="1974384889257048">
<meta property="og:title" content="<?php the_title(); ?>">
<meta property="og:description" content="<?php echo get_the_excerpt($post->ID); ?>">
<meta property="og:url" content="<?php the_permalink(); ?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full'); echo esc_attr($thumb[0]); ?>">
<meta property="og:site_name" content="<?php bloginfo('name'); ?> — <?php bloginfo('description'); ?>">