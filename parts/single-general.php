<!-- single -->
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<div class="col-12 single-buffer border-bottom">
		<!-- title -->
		<?php
			if(in_category('podcast')){
				?>
					<h6 class="text-uppercase article-category podcast-category">Podcast</h6>
				<?php
			}
		?>
		<h1 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		<?php
			if(has_post_thumbnail() && !in_category('podcast')){
				?>
					<div class="single-general-thumbnail-container">
						<?php the_post_thumbnail('full'); ?>
					</div>
				<?php
			}
		?>
		<!-- /title -->
		<!-- author and date -->
		<div class="media media-single">
			<img class="mr-3 rounded-circle" src="<?php echo get_avatar_url(get_the_author_meta('ID'),array('size'=>'38')); ?>">
			<div class="media-body">
				<p class="small meta-info-p"><strong><a class="author-url" href="<?php echo get_the_author_meta('user_url'); ?>"><?php echo get_the_author_meta('display_name'); ?></a></strong></p>
				<p class="small meta-info-p"><?php the_date(); ?></p>
			</div>
		</div>
		<!-- /author and date -->
		<!-- content -->
		<?php the_content(); ?>
		<!-- /content -->
	</div>
<?php endwhile; endif; ?>
<!-- /single -->