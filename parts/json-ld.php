<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "BlogPosting",
	"headline": "<?php the_title(); ?>",
	"description": "<?php echo get_the_excerpt($post->ID); ?>",
	"image": "<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full'); echo esc_attr($thumb[0]); ?>",
	"author": {
		"@type": "Person",
		"name": "<?php $author = $post->post_author; the_author_meta('display_name',$author); ?>"
	},
	"datePublished": "<?php echo get_the_date('Y-m-d',$post->ID); ?>",
	"publisher": {
		"@type": "Organization",
		"name": "<?php bloginfo('name'); ?> — <?php bloginfo('description'); ?>",
		"logo": "http://rideblog.mx/wp-content/themes/ride/img/ride_logo.png"
	},
	"dateModified": "<?php echo get_the_modified_date('Y-m-d',$post->ID); ?>",
	"mainEntityOfPage": {
		"@type": "WebPage",
		"@id": "<?php bloginfo('url'); ?>"
	}
}
</script>