<div id="ride-recent" class="container border-top">
	<div class="row destacados-buffer">
		<?php
			$destacados = new WP_Query(array(
				'category_name' => 'destacado',
				'posts_per_page' => '4'
			));

			if($destacados->have_posts()) : while($destacados->have_posts()) : $destacados->the_post();

			if(has_post_thumbnail()) {
				?>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="thumb-container">
							<?php echo the_post_thumbnail('destacado'); ?>
						</div>
						<h6 class="thumb-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
					</div>			
				<?php
			} else {
				?>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<h6 class="no-thumb-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
					</div>
				<?php
			}
		?>
		<?php endwhile; endif; wp_reset_postdata(); ?>
	</div>
</div>